package net.openid.conformance.extensions.yacs;

import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

public class ChainedEnhancedVerificationTests {

	@Test
	public void chainedVerificationPassesIfAllPass() throws IOException {

		EnhancedVerification first = mock(EnhancedVerification.class);
		EnhancedVerification second = mock(EnhancedVerification.class);
		when(first.allowed(any())).thenReturn(true);
		when(second.allowed(any())).thenReturn(true);
		EnhancedVerification chain = new ChainedEnhancedVerification(List.of(first, second));
		boolean allowed = chain.allowed(new BodyRepeatableHttpServletRequest(mock(HttpServletRequest.class)));

		assertTrue(allowed);

		verify(first).allowed(any());
		verify(second).allowed(any());

	}

	@Test
	public void chainedVerificationFailsIfOneFails() throws IOException {

		EnhancedVerification first = mock(EnhancedVerification.class);
		EnhancedVerification second = mock(EnhancedVerification.class);
		EnhancedVerification third = mock(EnhancedVerification.class);
		when(first.allowed(any())).thenReturn(true);
		when(second.allowed(any())).thenReturn(false);
		when(third.allowed(any())).thenReturn(true);
		EnhancedVerification chain = new ChainedEnhancedVerification(List.of(first, second, third));
		boolean allowed = chain.allowed(new BodyRepeatableHttpServletRequest(mock(HttpServletRequest.class)));

		assertFalse(allowed);

		verify(first).allowed(any());
		verify(second).allowed(any());
		verifyNoInteractions(third);

	}

}
