package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancelledFrom.EnsureCancelledFromWasDetentora;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class AbstractEnsureCancellationReasonWasXTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/patchPaymentsV2/patchPaymentWithCancellation2.json")
	public void validateGoodCancelledFrom() {
		EnsureCancelledFromWasDetentora condition = new EnsureCancelledFromWasDetentora();
		run(condition);
	}


	@Test
	@UseResurce("jsonResponses/paymentInitiation/patchPaymentsV2/patchPaymentWithCancellation.json")
	public void validateBadCancelledFrom() {
		EnsureCancelledFromWasDetentora condition = new EnsureCancelledFromWasDetentora();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Payment cancelledFrom type is not what was expected"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/patchPaymentsV2/patchPaymentNoCancellation.json")
	public void validateMissingCancelledFrom() {
		EnsureCancelledFromWasDetentora condition = new EnsureCancelledFromWasDetentora();
		ConditionError conditionError = runAndFail(condition);
		assertThat(conditionError.getMessage(), containsString("Could not extract cancelledFrom from the the resource_endpoint_response_full.data.cancellation"));
	}
}
