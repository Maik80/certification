package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

public class AddPaymentConsentRequestBodyToConfigTest {

	@Test
	public void constructConsentFromMinimalConfig() throws IOException, JSONException {

		JsonObject config = new JsonObjectBuilder()
			.addField("resource.loggedUserIdentification", "76109277673")
			.addField("resource.debtorAccountIspb", "12345678")
			.addField("resource.debtorAccountIssuer", "6272")
			.addField("resource.debtorAccountNumber", "94088392")
			.addField("resource.debtorAccountType", "CACC")
			.addField("resource.paymentAmount", "100.00")
			.build();



		Environment environment = new Environment();
		environment.putObject("config", config);

		AddPaymentConsentRequestBodyToConfig condition = new AddPaymentConsentRequestBodyToConfig();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		condition.evaluate(environment);

		JsonObject consent = environment.getElementFromObject("config", "resource.brazilPaymentConsent").getAsJsonObject();

		String expectedJson = IOUtils.resourceToString("payment_consent_request.json", Charset.defaultCharset(), getClass().getClassLoader());

		JSONAssert.assertEquals(expectedJson, consent.toString(), new CustomComparator(
			JSONCompareMode.LENIENT,
			new Customization("data.payment.date", (o1, o2) -> true)
		));

	}

	@Test
	public void constructConsentFromConfigWithBusinessEntity() throws IOException, JSONException {

		JsonObject config = new JsonObjectBuilder()
			.addField("resource.loggedUserIdentification", "76109277673")
			.addField("resource.debtorAccountIspb", "12345678")
			.addField("resource.debtorAccountIssuer", "6272")
			.addField("resource.debtorAccountNumber", "94088392")
			.addField("resource.debtorAccountType", "CACC")
			.addField("resource.paymentAmount", "100.00")
			.addField("resource.businessEntityIdentification", "123456789")
			.build();



		Environment environment = new Environment();
		environment.putObject("config", config);

		AddPaymentConsentRequestBodyToConfig condition = new AddPaymentConsentRequestBodyToConfig();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		condition.evaluate(environment);

		JsonObject consent = environment.getElementFromObject("config", "resource.brazilPaymentConsent").getAsJsonObject();

		String expectedJson = IOUtils.resourceToString("payment_consent_request_with_business_entity.json", Charset.defaultCharset(), getClass().getClassLoader());

		JSONAssert.assertEquals(expectedJson, consent.toString(), new CustomComparator(
			JSONCompareMode.LENIENT,
			new Customization("data.payment.date", (o1, o2) -> true)
		));

	}


	@Test
	public void failsIfMandatoryConfigMissing() throws IOException, JSONException {

		JsonObject config = new JsonObjectBuilder()
			.addField("resource.debtorAccountIspb", "12345678")
			.addField("resource.debtorAccountIssuer", "6272")
			.addField("resource.debtorAccountNumber", "94088392")
			.addField("resource.debtorAccountType", "CACC")
			.addField("resource.paymentAmount", "100.00")
			.build();

		Environment environment = new Environment();
		environment.putObject("config", config);

		AddPaymentConsentRequestBodyToConfig condition = new AddPaymentConsentRequestBodyToConfig();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		try {
			condition.evaluate(environment);
			fail("Should not pass");
		} catch (ConditionError ce) {
			assertEquals("AddPaymentConsentRequestBodyToConfig: Unable to find element resource.loggedUserIdentification in config", ce.getMessage());
		}

	}

}
