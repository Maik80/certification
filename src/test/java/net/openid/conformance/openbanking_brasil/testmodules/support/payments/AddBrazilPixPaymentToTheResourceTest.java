package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class AddBrazilPixPaymentToTheResourceTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_PATH_UNHAPPY = "jsonRequests/payments/consents/paymentConsentRequestNo";
	private static final String BASE_PATH_HAPPY = "jsonRequests/payments/consents/correctPaymentConsentRequest";

	@Before
	public void init() {
		JsonObject resource = new JsonObject();
		environment.putObject("resource", resource);
	}

	private void addRequestToEnvironment() {
		JsonObject resource = environment.getObject("resource");
		resource.add("brazilPaymentConsent", jsonObject);
	}

	@Test
	@UseResurce(BASE_PATH_HAPPY + "WithoutSchedule.json")
	public void happyWithoutSchedule() {
		addRequestToEnvironment();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);
	}

	@Test
	@UseResurce(BASE_PATH_HAPPY + "WithSchedule.json")
	public void happyWithSchedule() {
		addRequestToEnvironment();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		run(condition);
	}

	@Test
	@UseResurce(BASE_PATH_UNHAPPY + "Payment.json")
	public void unhappyNoPayment() {
		addRequestToEnvironment();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		assertThat(error.getMessage(), containsString("payment object is missing in the data"));
	}

	@Test
	@UseResurce(BASE_PATH_UNHAPPY + "Amount.json")
	public void unhappyNoAmount() {
		addRequestToEnvironment();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		assertThat(error.getMessage(), containsString("amount field is missing in the payment object"));
	}

	@Test
	@UseResurce(BASE_PATH_UNHAPPY + "Currency.json")
	public void unhappyNoCurrency() {
		addRequestToEnvironment();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		assertThat(error.getMessage(), containsString("currency field is missing in the payment object"));
	}

	@Test
	@UseResurce(BASE_PATH_UNHAPPY + "Type.json")
	public void unhappyNoType() {
		addRequestToEnvironment();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		assertThat(error.getMessage(), containsString("type field is missing in the payment object"));
	}

	@Test
	@UseResurce(BASE_PATH_UNHAPPY + "Details.json")
	public void unhappyNoDetails() {
		addRequestToEnvironment();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		assertThat(error.getMessage(), containsString("details object is missing in the payment"));
	}

	@Test
	@UseResurce(BASE_PATH_UNHAPPY + "LocalInstrument.json")
	public void unhappyNoLocalInstrument() {
		addRequestToEnvironment();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		assertThat(error.getMessage(), containsString("localInstrument field is missing in the details"));
	}

	@Test
	@UseResurce(BASE_PATH_UNHAPPY + "CreditorAccount.json")
	public void unhappyNoCreditorAccount() {
		addRequestToEnvironment();
		AddBrazilPixPaymentToTheResource condition = new AddBrazilPixPaymentToTheResource();
		ConditionError error = runAndFail(condition);

		assertThat(error.getMessage(), containsString("creditorAccount object is missing in the details"));
	}
}
