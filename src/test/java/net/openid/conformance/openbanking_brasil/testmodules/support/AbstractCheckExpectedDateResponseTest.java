package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.util.UseResurce;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.mockito.Mockito.mock;

public class AbstractCheckExpectedDateResponseTest extends AbstractJsonResponseConditionUnitTest {

	private CopyResourceEndpointResponse copyResourceEndpointResponse = new CopyResourceEndpointResponse();

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseOK.json")
	public void checkAccountsTransactionV1Response() {
		CheckExpectedBookingDateResponse condition = new CheckExpectedBookingDateResponse();
		environment.putString("fromBookingDate", "2021-01-01");
		environment.putString("toBookingDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseOK.json");
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/account/accountV2/transactionsV2/accountTransactionsResponseOK.json")
	public void checkAccountsTransactionV2Response() {
		CheckExpectedBookingDateResponse condition = new CheckExpectedBookingDateResponse();
		environment.putString("fromBookingDate", "2021-01-01");
		environment.putString("toBookingDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/accountV2/transactionsV2/accountTransactionsFullRangeResponseOK.json");
		run(condition);
	}

	private void copyAndAddFullRangeResponseToEnvironment(String path) {
		try {
			String fullResponseJson = IOUtils.resourceToString(path, StandardCharsets.UTF_8, getClass().getClassLoader());
			environment.putString("resource_endpoint_response_full", "body", fullResponseJson);
			TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
			copyResourceEndpointResponse.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
			copyResourceEndpointResponse.evaluate(environment);
			environment.mapKey("full_range_response", "resource_endpoint_response_full_copy");
		} catch (IOException e) {
			throw new AssertionError("Could not load resource");
		}
	}


}
