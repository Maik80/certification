package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.ValidateMetaOnlyRequestDateTimeDeprecated;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("jsonResponses/resourcesAPI/resourcesAPIResponseMultipleResources.json")
@Deprecated
public class ValidateMetaOnlyRequestDateTimeDeprecatedTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new ValidateMetaOnlyRequestDateTimeDeprecated());
	}

}
