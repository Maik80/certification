package net.openid.conformance.openbanking_brasil.opendata.exchange;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class ExchangeVetValueValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/opendata/exchange/ExchangeVetValueResponseV1.json")
	public void evaluate() {
		run(new ExchangeVetValueValidatorV1());
	}
}
