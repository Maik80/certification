package net.openid.conformance.raidiam.validators.referencesAuthority;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostAuthorityValidator}
 * Api url: ****
 * Api endpoint: GET /references/authorities/{AuthorityId}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory GET References - Authority by AuthorityId")
public class GetAuthorityByAuthorityIdValidator extends PostAuthorityValidator {
}
