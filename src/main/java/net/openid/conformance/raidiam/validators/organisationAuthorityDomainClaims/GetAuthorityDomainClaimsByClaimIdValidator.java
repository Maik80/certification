package net.openid.conformance.raidiam.validators.organisationAuthorityDomainClaims;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostAuthorityDomainClaimsValidator}
 * Api url: ****
 * Api endpoint: GET /organisations/{OrganisationId}/authoritydomainclaims/{OrganisationAuthorityDomainClaimId}
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory GET Authority Domain Claims by ClaimId")
public class GetAuthorityDomainClaimsByClaimIdValidator extends PostAuthorityDomainClaimsValidator {
}
