package net.openid.conformance.raidiam.validators.openIDProvider;

import net.openid.conformance.logging.ApiName;

/**
 * Api url: ****
 * Api endpoint: POST /device/auth
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory POST Device Auth")
public class PostDeviceAuthValidator extends PostTokenValidator {
}
