package net.openid.conformance.raidiam.validators.openIDProvider;

import net.openid.conformance.logging.ApiName;

/**
 * Api url: ****
 * Api endpoint: POST /request
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory POST Request")
public class PostRequestValidator extends PostBackChannelValidator {
}
