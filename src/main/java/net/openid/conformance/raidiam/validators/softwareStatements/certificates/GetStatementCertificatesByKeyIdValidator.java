package net.openid.conformance.raidiam.validators.softwareStatements.certificates;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: GET /organisations/{OrganisationId}/softwarestatements/{SoftwareStatementId}/certificates/{SoftwareStatementCertificateOrKeyType}/{CertificateOrKeyId}
 */
@ApiName("Raidiam Directory GET Software Statement Certificates by CertificateOrKeyId")
public class GetStatementCertificatesByKeyIdValidator extends PostStatementCertificatesValidator {
}
