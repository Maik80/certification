package net.openid.conformance.raidiam.validators.softwareStatements.authorityClaims;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: GET /organisations/{OrganisationId}/softwarestatements/{SoftwareStatementId}/authorityclaims
 */
@ApiName("Raidiam Directory GET Software Statement Authority Claims By ClaimsID")
public class GetStatementAuthorityClaimsByClaimsIdValidator extends PostStatementAuthorityClaimsValidator {
}
