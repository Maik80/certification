package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ResponseTypeAdder implements DcrRequestProcessor {

	public static DcrRequestProcessor INSTANCE = new ResponseTypeAdder();

	@Override
	public void process(JsonObject dcrRequest) {
		JsonArray responseTypes = new JsonArray();
		responseTypes.add("code id_token");
		dcrRequest.add("response_types" ,responseTypes);
	}

}
