package net.openid.conformance.extensions.yacs.dcr;

import java.util.Optional;
import java.util.Properties;

public class DirectoryDetails {

	private static final String SS_URI_TEMPLATE = "%s/organisations/%s/softwarestatements/%s/assertion";

	private static final String ORG_ID_PROP = "credentials.org-id";
	private static final String SS_ID_PROP = "credentials.software-statement-id";
	private static final String DIRECTORY_DISCOVERY_URI_PROP = "directory.discovery-uri";
	private static final String DIRECTORY_BASE_URI_PROP = "directory.base-uri";
	private static final String DIRECTORY_KEYSTORE_URI_PROP = "directory.keystore-base";

	private String directoryUri;
	private String discoveryUri;
	private String orgId;
	private String softwareStatementId;
	private String keystoreUri;

	public DirectoryDetails(Properties properties) {
		this.directoryUri = findIfPresent(DIRECTORY_BASE_URI_PROP, properties);
		this.discoveryUri = findIfPresent(DIRECTORY_DISCOVERY_URI_PROP, properties);
		this.keystoreUri = findIfPresent(DIRECTORY_KEYSTORE_URI_PROP, properties);
		this.orgId = findIfPresent(ORG_ID_PROP, properties);
		this.softwareStatementId = findIfPresent(SS_ID_PROP, properties);
	}

	public String getDiscoveryUri() {
		return discoveryUri;
	}

	public String getOrgId() {
		return orgId;
	}

	public String getSoftwareStatementId() {
		return softwareStatementId;
	}

	public String softwareStatementUri() {
		return String.format(SS_URI_TEMPLATE, directoryUri,
			orgId, softwareStatementId);
	}

	public String getKeystoreUri() {
		return keystoreUri;
	}

	private String findIfPresent(String key, Properties properties) {
		return Optional.ofNullable(properties.getProperty(key)).orElseThrow(() -> new RuntimeException(String.format("Property %s not found", key)));
	}
}
