package net.openid.conformance.extensions.yacs;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PlanCreationBlockingFilter extends AbstractYACSFilter {

	private static final Logger LOG = LoggerFactory.getLogger(PlanCreationBlockingFilter.class);

	private final EnhancedVerification enhancedVerification;

	public PlanCreationBlockingFilter() {
		this(EnhancedVerification.ALWAYS);
	}

	public PlanCreationBlockingFilter(EnhancedVerification enhancedVerification) {
		super(new MethodAndPathMatch("POST", "/api/plan"));
		this.enhancedVerification = enhancedVerification;
	}

	@Override
	public void processRequest(BodyRepeatableHttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		LOG.info("New plan being created - performing enhanced verification of user");
		if(enhancedVerification.allowed(request)) {
			LOG.info("User verified - proceeding");
			return;
		}
		LOG.info("User not verified - preventing");
		response.setHeader("X-FAPI-Forbidden-Explain-Link", "/fvpPlanCreationRestrictions.html");
		response.setHeader("X-FAPI-Forbidden-Explain-Linktext", "See here for more details");
		response.sendError(HttpStatus.SC_FORBIDDEN, String.format("You are not permitted to create this test plan as it targets an authorisation server not owned by your organisation or you are using a national identity which is not your own."));

	}
}
