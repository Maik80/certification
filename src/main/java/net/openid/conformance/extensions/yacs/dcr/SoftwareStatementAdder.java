package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;

public class SoftwareStatementAdder implements DcrRequestProcessor {

	private String softwareStatement;

	public SoftwareStatementAdder(String softwareStatement) {
		this.softwareStatement = softwareStatement;
	}

	@Override
	public void process(JsonObject dcrRequest) {
		dcrRequest.addProperty("software_statement", softwareStatement);
	}
}
