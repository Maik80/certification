package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.TemporalAmount;
import java.util.Map;

public class OrphanCrushingMachine {

	private static final TemporalAmount FIFTEEN_MINUTES = Duration.ofMinutes(15);
	private static final Logger LOG = LoggerFactory.getLogger(OrphanCrushingMachine.class);

	private DcrClientRepository clientRepository;
	private DcrHelper dcrHelper;

	public OrphanCrushingMachine(DcrClientRepository clientRepository, DcrHelper dcrHelper) {
		this.clientRepository = clientRepository;
		this.dcrHelper = dcrHelper;
	}

	@Scheduled(fixedRateString = "300000")
	public void emptyParticipantsCache() {
		LOG.info("Removing orphaned DCR clients");

		Map<String, JsonObject> clients = clientRepository.clientsOlderThan(Instant.now().minus(FIFTEEN_MINUTES));
		crushOrphans(clients);
		LOG.info("Orphaned DCR clients removed");
	}

	private void crushOrphans(Map<String, JsonObject> clients) {
		for(Map.Entry<String, JsonObject> entry: clients.entrySet()) {
			JsonObject client = entry.getValue();
			String clientId = OIDFJSON.getString(client.get("client_id"));
			String testId = entry.getKey();
			try {
				dcrHelper.unregisterClient(client);
				clientRepository.deleteSavedClient(testId);
				LOG.info("Unregistered orphaned client {} from test {}", clientId, testId);
			} catch (ClientDeletionException ex) {
				LOG.error("Problem deleting pre-roll DCR client {} for test {}", clientId, testId);
			}
		}
	}

	public static class ShutdownHook implements Runnable {

		private final OrphanCrushingMachine orphanCrushingMachine;
		private final DcrClientRepository repository;

		public ShutdownHook(DcrClientRepository clientRepository, OrphanCrushingMachine orphanCrushingMachine) {
			this.repository = clientRepository;
			this.orphanCrushingMachine = orphanCrushingMachine;
		}

		@Override
		public void run() {
			LOG.info("Application shutting down - remove as many clients as we can");
			Map<String, JsonObject> clients = repository.clientsOlderThan(Instant.now());
			orphanCrushingMachine.crushOrphans(clients);
		}
	}

}
