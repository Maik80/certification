package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.util.StreamUtils;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class BodyRepeatableHttpServletRequest extends HttpServletRequestWrapper {

	private byte[] cachedBody;

	private JsonObject jsonObject;

	public BodyRepeatableHttpServletRequest(ServletRequest request) throws IOException {
		this((HttpServletRequest) request);
	}

	public BodyRepeatableHttpServletRequest(HttpServletRequest request) throws IOException {
		super(request);
		InputStream requestInputStream = request.getInputStream();
		this.cachedBody = StreamUtils.copyToByteArray(requestInputStream);
		if(cachedBody != null && cachedBody.length > 0) {
			jsonObject = (JsonObject) JsonParser.parseString(new String(cachedBody));
		}
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		return new CachedBodyServletInputStream(this.cachedBody);
	}

	public JsonObject body() {
		return jsonObject;
	}

	public void updateBody(JsonObject jsonObject) {
		this.jsonObject = jsonObject;
		this.cachedBody = jsonObject.toString().getBytes(StandardCharsets.UTF_8);
	}
}
