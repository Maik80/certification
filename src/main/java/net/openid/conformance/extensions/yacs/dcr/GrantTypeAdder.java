package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class GrantTypeAdder implements DcrRequestProcessor {

	public static final DcrRequestProcessor INSTANCE = new GrantTypeAdder();

	@Override
	public void process(JsonObject dcrRequest) {
		JsonArray grantTypes = new JsonArray();
		grantTypes.add("authorization_code");
		grantTypes.add("implicit");
		grantTypes.add("refresh_token");
		grantTypes.add("client_credentials");
		dcrRequest.add("grant_types", grantTypes);
	}

}
