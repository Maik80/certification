package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.nimbusds.jwt.JWT;
import net.openid.conformance.extensions.yacs.dcr.DirectoryDetails;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class ConfigFilter extends AbstractYACSFilter {

	private ParticipantsService participantsService;
	private AuthenticationFacade authenticationFacade;

	private final DirectoryDetails directoryDetails;

	private final Logger logger = LoggerFactory.getLogger(ConfigFilter.class);
	@Value("${fintechlabs.yacs.directory.uri}")
	private String participantsApiUri;

	public ConfigFilter(ParticipantsService participantsService, AuthenticationFacade authenticationFacade, DirectoryDetails directoryDetails) {
		super(new MethodAndPathMatch("POST", "/api/plan"));
		this.participantsService = participantsService;
		this.authenticationFacade = authenticationFacade;
		this.directoryDetails = directoryDetails;
	}

	@Override
	public void processRequest(BodyRepeatableHttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		JsonObject body = request.body();
		inferOrgId(body);
		if (planNameContains(request.getParameterValues("planName"),"Open Finance Brasil Functional Production Tests - FVP Phase 3 - Payments")){
			logger.info("Payments test plan, inserting Brasil consent");
			insertBrasilConsent(body);
		}
		insertDirectoryObject(body);
		request.updateBody(body);
	}

	private void inferOrgId(JsonObject config) {
		JsonObject serverConfig = config.getAsJsonObject("server");
		if(serverConfig == null) {
			return;
		}
		String wellKnown = OIDFJSON.getString(serverConfig.get("discoveryUrl"));
		OIDCAuthenticationToken auth = (OIDCAuthenticationToken) authenticationFacade.getContextAuthentication();
		JWT idToken = auth.getIdToken();
		try {
			JSONObject trustFrameworkProfile = (JSONObject) idToken.getJWTClaimsSet().getClaim("trust_framework_profile");
			if(trustFrameworkProfile == null) {
				return;
			}
			JSONObject orgAccessDetails = (JSONObject) trustFrameworkProfile.get("org_access_details");
			if(orgAccessDetails == null) {
				return;
			}
			Set<String> orgIds = orgAccessDetails.keySet();
			Set<Map> orgs = participantsService.orgsFor(orgIds);
			for(Map org: orgs) {
				String orgId = String.valueOf(org.get("OrganisationId"));
				List<Map> authServersForOrg = (List<Map>) org.get("AuthorisationServers");
				for(Map authServer: authServersForOrg) {
					String oidcDiscovery = String.valueOf(authServer.get("OpenIDDiscoveryDocument"));
					if(oidcDiscovery.equals(wellKnown)) {
						config.addProperty("alias", orgId);
						insertOrgId(config, orgId);
						return;
					}
				}

			}

		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	private void insertOrgId(JsonObject config, String organisationId){
		if (config.keySet().contains("resource")){
			JsonObject resource = config.getAsJsonObject("resource");
			resource.addProperty("brazilOrganizationId",organisationId);
		} else {
			JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder();
			jsonObjectBuilder.addField("brazilOrganizationId",organisationId);

			JsonObject resource = jsonObjectBuilder.build();
			config.add("resource",resource);
		}
	}
	private void insertDirectoryObject(JsonObject config){
		JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
			.addField("participants",participantsApiUri)
			.addField("keystore", directoryDetails.getKeystoreUri().concat("/"))
			.addField("apibase", directoryDetails.getDiscoveryUri());

		JsonObject newConfigObject = jsonObjectBuilder.build();

		config.add("directory", newConfigObject);
	}

	private void insertBrasilConsent(JsonObject config){
		logger.info("Preparing to inject Brasil consent into config");
		JsonObject resource = config.getAsJsonObject("resource");
		String brasilCpf = getStringFromObject(resource,"brazilCpf");
		String businessEntityIdentification = getStringFromObject(resource,"brazilCnpj");
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

		Random random = new Random();
		float leftLimit = 0.01F;
		float rightLimit = 1F;
		double generatedFloat = leftLimit + random.nextFloat() * (rightLimit - leftLimit);

		JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
			.addFields("data.loggedUser.document", Map.of(
				"identification", String.valueOf(brasilCpf),
				"rel", "CPF")
			)
			.addFields("data.creditor",
				Map.of(
					"personType", DictHomologKeys.PROXY_PRODUCTION_PERSON_TYPE,
					"cpfCnpj", DictHomologKeys.PROXY_PRODUCTION_CPF,
					"name", DictHomologKeys.PROXY_PRODUCTION_NAME)
			)
			.addFields("data.payment", Map.of(
				"type", "PIX",
				"currency", DictHomologKeys.PROXY_EMAIL_STANDARD_CURRENCY,
				"amount", String.format("%.2f", generatedFloat),
				"date", currentDate.toString())
			)
			.addFields("data.payment.details", Map.of(
				"localInstrument", DictHomologKeys.PROXY_EMAIL_STANDARD_LOCALINSTRUMENT,
				"proxy", DictHomologKeys.PROXY_PRODUCTION_PROXY)
			)
			.addFields("data.payment.details.creditorAccount", Map.of(
				"ispb", DictHomologKeys.PROXY_PRODUCTION_ISPB,
				"issuer", DictHomologKeys.PROXY_PRODUCTION_BRANCH_NUMBER,
				"number", DictHomologKeys.PROXY_PRODUCTION_ACCOUNT_NUMBER,
				"accountType", DictHomologKeys.PROXY_PRODUCTION_ACCOUNT_TYPE)
			);

		if (businessEntityIdentification != null){
			jsonObjectBuilder
				.addFields("data.businessEntity.document", Map.of(
					"identification", businessEntityIdentification,
					"rel", "CNPJ"
				));
		}

		JsonObject consentRequest = jsonObjectBuilder.build();

		resource.add("brazilPaymentConsent",consentRequest);

		config.add("resource",resource);
		logger.info("Brasil consent added to config");
	}

	private String getStringFromObject(JsonObject jsonObject, String key){
		if (jsonObject.keySet().contains(key) && jsonObject.get(key).isJsonPrimitive()){
			return OIDFJSON.getString(jsonObject.get(key));
		}
		return null;
	}


	private boolean planNameContains(String[] planNameParameters, String planName){
		for (String parameter: planNameParameters) {
			if (parameter.equals(planName)){
				return true;
			}
		}
		return false;
	}
}

