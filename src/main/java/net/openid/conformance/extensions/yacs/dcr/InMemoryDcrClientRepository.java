package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class InMemoryDcrClientRepository implements DcrClientRepository {

	private static final Logger LOG = LoggerFactory.getLogger(InMemoryDcrClientRepository.class);

	private Map<String, ClientHolder> clients = new HashMap<>();

	@Override
	public void saveClient(String testId, JsonObject clientConfig) {
		String clientId = OIDFJSON.getString(clientConfig.get("client_id"));
		LOG.info("Adding DCR-obtained client {} for test {}", clientId, testId);
		clients.put(testId, new ClientHolder(clientConfig));
	}

	@Override
	public JsonObject getSavedClient(String testId) {
		return Optional.ofNullable(clients.get(testId))
			.map(ClientHolder::getDynamicClient)
			.orElse(null);
	}

	@Override
	public Map<String, JsonObject> clientsOlderThan(Instant instant) {
		Map<String, JsonObject> orphanedClients = clients.entrySet()
			.stream()
			.filter(e -> e.getValue().createdAt.isBefore(instant))
			.collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue().dynamicClient));
		return orphanedClients;
	}

	@Override
	public void deleteSavedClient(String id) {
		ClientHolder removed = clients.remove(id);
		JsonObject client = removed.dynamicClient;
		String clientId = OIDFJSON.getString(client.get("client_id"));
		LOG.info("Removing DCR-obtained client {} for test {}", clientId, id);
	}


	private static class ClientHolder {

		private Instant createdAt = Instant.now();
		private JsonObject dynamicClient;

		public ClientHolder(JsonObject clientConfig) {
			this.dynamicClient = clientConfig;
		}

		public Instant getCreatedAt() {
			return createdAt;
		}

		public JsonObject getDynamicClient() {
			return dynamicClient;
		}
	}

}
