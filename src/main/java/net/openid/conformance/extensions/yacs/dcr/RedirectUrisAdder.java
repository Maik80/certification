package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

public class RedirectUrisAdder implements DcrRequestProcessor {

	private final List<String> redirects;

	public RedirectUrisAdder(String redirect) {
		this(List.of(redirect));
	}

	public RedirectUrisAdder(List<String> redirects) {
		this.redirects = redirects;
	}

	@Override
	public void process(JsonObject dcrRequest) {
		JsonArray redirectUris = new JsonArray();
		redirects.stream()
				.forEach(it -> redirectUris.add(it));
		dcrRequest.add("redirect_uris" ,redirectUris);
	}

}
