package net.openid.conformance.condition.client;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class AddClientSpecificInformationToClientConfigurationRequest extends AbstractCondition {

	@Override
	@PreEnvironment(required = {"client", "registration_client_endpoint_request_body"})
	@PostEnvironment(required = "registration_client_endpoint_request_body")
	public Environment evaluate(Environment env) {
		JsonObject requestBody = env.getObject("registration_client_endpoint_request_body");
		JsonObject client = env.getObject("client");

		// client_id is mandatory
		JsonElement clientId = client.get("client_id");
		if (clientId == null) {
			throw error("The client does not have a client_id", args("client", client));
		}
		requestBody.addProperty("client_id", OIDFJSON.getString(clientId));

		JsonElement clientName = client.get("client_name");
		if (clientName != null) {
			requestBody.addProperty("client_name", OIDFJSON.getString(clientName));
		}

		JsonElement clientUri = client.get("client_uri");
		if (clientUri != null) {
			requestBody.addProperty("client_uri", OIDFJSON.getString(clientUri));
		}

		logSuccess("Added client specific information to client configuration request body", args("requestBody", requestBody));

		return env;
	}
}
