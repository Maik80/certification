package net.openid.conformance.condition.client;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class ValidateClientJWKsPrivatePart extends AbstractValidateJWKs {

	@Override
	@PreEnvironment(required = "client")
	public Environment evaluate(Environment env) {
		JsonElement jwks = env.getElementFromObject("client", "jwks");

		// The below is a quick hack to allow prod-tpp instances to bypass the validation of
		// private key JWK, as this involves going back to KMS to do so
		boolean verifyPrivatePart = true;
		JsonObject config = env.getObject("config");
		if(config != null) {
			config = config.getAsJsonObject("mtls");
		}
		if(config != null) {
			verifyPrivatePart = !config.has("mtls_alias");
		}
		checkJWKs(jwks, verifyPrivatePart);

		logSuccess("Valid client JWKs: keys are valid JSON, contain the required fields, the private/public exponents match and are correctly encoded using unpadded base64url");

		return env;
	}
}
