package net.openid.conformance.util.field;

public class ExtraField extends Field {

	private ExtraField(String pattern, int maxLength, int minLength, boolean isMustNotBePresent) {
		super(pattern, maxLength, minLength, isMustNotBePresent);
	}

	public static class Builder {

		private String pattern = "";
		private int maxLength;
		private int minLength;
		private boolean isMustNotBePresent = false;



		public Builder setPattern(String pattern) {
			this.pattern = pattern;
			return this;
		}

		public Builder setMaxLength(int maxLength) {
			this.maxLength = maxLength;
			return this;
		}

		public Builder setMinLength(int minLength) {
			this.minLength = minLength;
			return this;
		}

		public Builder setMustNotBePresent() {
			this.isMustNotBePresent = true;
			return this;
		}

		public ExtraField build() {
			return new ExtraField(this.pattern, this.maxLength, this.minLength, this.isMustNotBePresent);
		}
	}
}
