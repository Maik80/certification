package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collections;

public class CallDirectoryRootsEndpoint extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String directoryRootsEndpoint = getStringFromEnvironment(env,"config","directory.directoryRootsUri", "Directory Roots Endpoint");
		try {
			RestTemplate restTemplate = createRestTemplate(env);

			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public boolean hasError(ClientHttpResponse response) {
					// Treat all http status codes as 'not an error', so spring never throws an exception due to the http
					// status code meaning the rest of our code can handle http status codes how it likes
					return false;
				}
			});

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setAcceptCharset(Collections.singletonList(StandardCharsets.UTF_8));
			HttpMethod httpMethod = HttpMethod.GET;
			HttpEntity<?> request = new HttpEntity<>(headers);

			try {
				ResponseEntity<String> response = restTemplate.exchange(directoryRootsEndpoint, httpMethod, request, String.class);
				JsonObject responseInfo = convertResponseForEnvironment("directory roots uri", response);

				env.putObject("directory_roots_response", responseInfo);

				logSuccess("Successfully called Directory Roots Uri");

			} catch (RestClientResponseException e) {
				throw error("Error from directory roots uri",
					args("directory roots uri", directoryRootsEndpoint, "code", e.getRawStatusCode(),
						"status", e.getStatusText(), "body", e.getResponseBodyAsString()));

			} catch (RestClientException e) {

				String reason = "Unknown";
				if (e.getCause() != null) {
					reason = e.getCause().getMessage();
				}

				throw error("Call to directory roots uri failed", e,
					args("directory roots uri", directoryRootsEndpoint, "reason", reason));
			}


		} catch (NoSuchAlgorithmException | KeyManagementException | CertificateException | InvalidKeySpecException |
				 KeyStoreException | IOException | UnrecoverableKeyException e) {
			throw error("Error creating HTTP Client", e);
		}
		return env;	}
}
