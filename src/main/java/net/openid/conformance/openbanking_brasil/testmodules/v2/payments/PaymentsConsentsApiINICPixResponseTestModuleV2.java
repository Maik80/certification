package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddTransactionIdentification;
import net.openid.conformance.openbanking_brasil.testmodules.support.RemoveQRCodeFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectINICCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectINICCodePixLocalInstrument;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments-api-inic-pix-response-test-v2",
	displayName = "Payments API test module for inic local instrument pix response",
	summary = "Ensure response are valid when localInstrument is INIC\n" +
		"\u2022 Calls POST Consents Endpoint with localInstrument as INIC\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"n" +
		"\u2022 Calls the POST Payments with localInstrument as INIC, with a transactionIdentifier on the payload\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expectes Definitive  state (ACSC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiINICPixResponseTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {


	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EnsureProxyPresentInConfig.class);
		callAndStopOnFailure(AddTransactionIdentification.class);
		callAndStopOnFailure(SelectINICCodeLocalInstrument.class);
		callAndStopOnFailure(SelectINICCodePixLocalInstrument.class);
		callAndStopOnFailure(RemoveQRCodeFromConfig.class);
	}


}
