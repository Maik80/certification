package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ForceToValidateConsentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public abstract class AbstractPermissionsCheckingFunctionalTestModule extends AbstractOBBrasilFunctionalTestModule {

	private boolean isFirstConsent = true;

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(RememberOriginalScopes.class);
		prepareCorrectConsents();
	}

	protected boolean preFetchResources = false;

	@Override
	protected void requestProtectedResource() {
		if(!preFetchResources) {
			preFetchResources = true;
			super.requestProtectedResource();
			eventLog.startBlock(currentClientString() + "Validate response");
			preFetchResources();
			callAndStopOnFailure(ResetScopesToConfigured.class);
			prepareIncorrectPermissions();
			performAuthorizationFlow();
			eventLog.endBlock();
		}
	}

	boolean preFetched = false;

	@Override
	protected void onPostAuthorizationFlowComplete() {

		if(!preFetched) {
			preFetched = true;
			return;
		}

		requestResourcesWithIncorrectPermissions();

		fireTestFinished();

	}

	@Override
	protected void performPreAuthorizationSteps() {
		if (!isFirstConsent) {
			deleteConsent();
		} else {
			isFirstConsent = false;
		}
		callAndStopOnFailure(ForceToValidateConsentResponse.class);
		super.performPreAuthorizationSteps();
		callAndContinueOnFailure(SaveConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
	}

	private void deleteConsent() {
		eventLog.startBlock("Deleting consent");
		callAndContinueOnFailure(LoadConsentsAccessToken.class);
		callAndContinueOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	public void cleanup() {
		deleteConsent();
	}

	protected abstract void preFetchResources();
	protected abstract void prepareCorrectConsents();
	protected abstract void prepareIncorrectPermissions();

	protected abstract void requestResourcesWithIncorrectPermissions();

	@Override
	protected final void validateResponse() {

	}

}
