package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EnsureBrazilCpf extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate (Environment env) {
		String brasilCpf = env.getString("config", "resource.brazilCpf");

		if(Strings.isNullOrEmpty(brasilCpf)) {
			throw error("brazilCpf is missing.");
		}

		logSuccess("brazilCpf was successfully found.");
		return env;
	}
}
