package net.openid.conformance.openbanking_brasil.testmodules.support;

public class AddUnarrangedOverdraftScope extends AbstractScopeAddingCondition {
	@Override
	protected String newScope() {
		return "unarranged-accounts-overdraft";
	}
}
