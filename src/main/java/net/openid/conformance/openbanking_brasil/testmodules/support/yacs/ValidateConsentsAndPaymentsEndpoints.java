package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import java.util.HashMap;
import java.util.Map;

public class ValidateConsentsAndPaymentsEndpoints extends AbstractValidateEndpointExistsForAs{
	@Override
	protected Map<String, Map<String, String>> getEndpoints() {
		Map<String, Map<String, String>> outerMap = new HashMap<>();
		Map<String, String> consentsInnerMap = new HashMap<>();
		Map<String, String> resourcesInnerMap = new HashMap<>();
		consentsInnerMap.put("endpoint", "payments/v2/consents");
		outerMap.put("payments-consents", consentsInnerMap);
		resourcesInnerMap.put("endpoint", "payments/v2/pix/payments");
		outerMap.put("payments-pix", resourcesInnerMap);
		return outerMap;
	}
	@Override
	protected String getVersionRegex() {
		String versionValidatorRegex= "^(2.[0-9].[0-9])$";
		return versionValidatorRegex;
	}
}
