package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.CreatePaymentErrorValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.ValidateMetaOnlyRequestDateTime;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractPaymentUnhappyPathTestModuleV2 extends AbstractOBBrasilQrCodePaymentFunctionalTestModuleV2 {


	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.replace(EnsureResponseCodeWas201.class, condition(getExpectedPaymentResponseCode()));
	}

	@Override
	protected void validateResponse() {
		int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));

		env.mapKey(ValidateMetaOnlyRequestDateTime.RESPONSE_ENV_KEY, "resource_endpoint_response_full");

		if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			perform422ResponseActions();
		} else {
			performNon422ResponseActions();
		}


	}

	protected void perform422ResponseActions() {
		eventLog.log(getName(), "Validating 422 response");
		performErrorValidation();
		validate422ErrorResponseCode();
	}

	protected void performErrorValidation() {
		env.putBoolean(ValidateMetaOnlyRequestDateTime.IS_META_OPTIONAL, true);
		callAndStopOnFailure(CreatePaymentErrorValidatorV2.class);
		callAndStopOnFailure(ValidateMetaOnlyRequestDateTime.class);
	}

	protected void performNon422ResponseActions() {
		super.validateResponse();
		validatePaymentRejectionReasonCode();
	}

	@Override
	protected void validateFinalState() {
		callAndStopOnFailure(EnsurePaymentStatusWasRjct.class);
	}


	/**
	 * Used  if payments response was 201
	 */
	protected abstract void validatePaymentRejectionReasonCode();

	/**
	 * Used  if payments response was 422
	 */
	protected abstract void validate422ErrorResponseCode();

	protected abstract Class<? extends Condition> getExpectedPaymentResponseCode();

}
