package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import net.openid.conformance.condition.client.FAPIBrazilCallPaymentConsentEndpointWithBearerToken;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Collections;


public class FAPIBrazilCallPaymentConsentEndpointWithBearerTokenNoAcceptField extends FAPIBrazilCallPaymentConsentEndpointWithBearerToken {
	@Override
	protected HttpHeaders getHeaders(Environment env) {
		HttpHeaders headers = super.getHeaders(env);
		//Spring internally adds accept field, therefore we set it to ALL
		headers.setAccept(Collections.singletonList(MediaType.ALL));
		return headers;
	}
}
