package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddAudAsProtectedResourceUrlToRequestObject extends AbstractCondition {

	@Override
	@PreEnvironment(required = "request_object_claims", strings = "protected_resource_url")
	public Environment evaluate(Environment env) {
		JsonObject requestObjectClaims = env.getObject("request_object_claims");
		String protectedResourceEndpoint = env.getString("protected_resource_url");
		requestObjectClaims.addProperty("aud", protectedResourceEndpoint);
		logSuccess("Added aud to request object claims", args("aud", protectedResourceEndpoint));

		return env;
	}
}
