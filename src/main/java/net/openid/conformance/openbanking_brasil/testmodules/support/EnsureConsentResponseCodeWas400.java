package net.openid.conformance.openbanking_brasil.testmodules.support;

import org.springframework.http.HttpStatus;

public class EnsureConsentResponseCodeWas400 extends AbstractEnsureConsentResponseCode {

	@Override
	protected int getExpectedStatus() {
		return HttpStatus.BAD_REQUEST.value();
	}
}
