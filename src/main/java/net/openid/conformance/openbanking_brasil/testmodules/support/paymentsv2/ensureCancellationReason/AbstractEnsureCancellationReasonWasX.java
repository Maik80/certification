package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public abstract class AbstractEnsureCancellationReasonWasX extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			String cancellationReason = OIDFJSON.getString(BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.flatMap(responseBody -> Optional.ofNullable(responseBody.getAsJsonObject().getAsJsonObject("data")))
				.flatMap(data -> Optional.ofNullable(data.getAsJsonObject("cancellation")))
				.flatMap(cancellation -> Optional.ofNullable(cancellation.get("reason")))
				.orElseThrow(() -> error("Could not extract cancellationReason from the the resource_endpoint_response_full.data.cancellation")));

			List<String> expectedCancellationReasons = getExpectedCancellationReasons();

			if (!expectedCancellationReasons.contains(cancellationReason)) {
				throw error("Payment cancellationReason type is not what was expected",
					args("Expected", expectedCancellationReasons, "Actual", cancellationReason));
			}

			logSuccess("Received expected cancellationReason type",
				args("Expected", expectedCancellationReasons, "Received", cancellationReason));

		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected abstract List<String> getExpectedCancellationReasons();
}
