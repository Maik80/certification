package net.openid.conformance.openbanking_brasil.testmodules.support;

public class ResourceErrorMetaValidator extends AbstractErrorMetaValidator {
	@Override
	protected boolean isResource() {
		return true;
	}
}
