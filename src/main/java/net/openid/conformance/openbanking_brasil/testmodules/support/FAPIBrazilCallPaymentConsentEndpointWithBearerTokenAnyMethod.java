package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.client.FAPIBrazilCallPaymentConsentEndpointWithBearerToken;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpMethod;

public class FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod extends FAPIBrazilCallPaymentConsentEndpointWithBearerToken {

	@Override
	protected String getUri(Environment env) {
		HttpMethod httpMethod = getMethod(env);
		if (!httpMethod.equals(HttpMethod.POST)) {
			String consentUrl = env.getString("consent_url");
			if (Strings.isNullOrEmpty(consentUrl)) {
				throw error("consent url missing from configuration");
			}
			return consentUrl;
		}

		return super.getUri(env);
	}

	@Override
	protected HttpMethod getMethod(Environment env) {
		String method = env.getString("http_method");
		if (Strings.isNullOrEmpty(method)) {
			throw error("HTTP method not found");
		}
		return HttpMethod.valueOf(method);
	}

	@Override
	protected Object getBody(Environment env) {
		HttpMethod httpMethod = getMethod(env);
		if (httpMethod.equals(HttpMethod.GET) || httpMethod.equals(HttpMethod.DELETE)) {
			return null;
		}
		return super.getBody(env);
	}
}
