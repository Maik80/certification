package net.openid.conformance.openbanking_brasil.testmodules.support;

import org.springframework.http.HttpStatus;

public class EnsureConsentResponseCodeWas422 extends AbstractEnsureConsentResponseCode {

	@Override
	protected int getExpectedStatus() {
		return HttpStatus.UNPROCESSABLE_ENTITY.value();
	}
}
