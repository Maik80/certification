package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import java.util.HashMap;
import java.util.Map;

public class ValidateConsentsAndResourcesEndpoints extends AbstractValidateEndpointExistsForAs{
	@Override
	protected Map<String, Map<String, String>> getEndpoints() {
		Map<String, Map<String, String>> outerMap = new HashMap<>();
		Map<String, String> consentsInnerMap = new HashMap<>();
		Map<String, String> resourcesInnerMap = new HashMap<>();
		consentsInnerMap.put("endpoint", "/consents/v2/consents");
		outerMap.put("consents", consentsInnerMap);
		resourcesInnerMap.put("endpoint", "/resources/v2/resources");
		outerMap.put("resources", resourcesInnerMap);
		return outerMap;
	}
	@Override
	protected String getVersionRegex() {
		String versionValidatorRegex= "^(2.[0-9].[0-9])$";
		return versionValidatorRegex;
	}
}
