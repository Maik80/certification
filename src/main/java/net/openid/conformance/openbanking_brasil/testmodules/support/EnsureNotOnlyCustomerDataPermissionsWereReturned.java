package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class EnsureNotOnlyCustomerDataPermissionsWereReturned extends AbstractCondition {

	private static final Set<String> CUSTOMER_DATA_PERMISSIONS = Set.of("CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ", "CUSTOMERS_PERSONAL_ADITTIONALINFO_READ",
		"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ", "CUSTOMERS_BUSINESS_ADITTIONALINFO_READ","RESOURCES_READ");

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		String bodyJson = OIDFJSON.getString(env.getElementFromObject("consent_endpoint_response_full", "body"));
		Gson gson = JsonUtils.createBigDecimalAwareGson();
		JsonObject body = gson.fromJson(bodyJson, JsonObject.class);

		JsonElement data = Optional.ofNullable(body.get("data"))
			.orElseThrow(() -> error("Could not find data in the consent response", args("response", body)));

		JsonElement grantedPermissionsEl = Optional.ofNullable(data.getAsJsonObject().get("permissions"))
			.orElseThrow(() -> error("Couldn't find permissions array in the consent response", args("response", body)));

		if (!grantedPermissionsEl.isJsonArray()) {
			throw error("permissions element in the consent response is not a JSON array", args("permissions", grantedPermissionsEl));
		}
		JsonArray grantedPermissions = grantedPermissionsEl.getAsJsonArray();
		if (grantedPermissions.size() <= 0) {
			throw error("Permissions array in the consent response is empty", args("permissions", grantedPermissionsEl));
		}

		List<String> extraPermissions = new LinkedList<>();
		grantedPermissions.iterator().forEachRemaining(el -> {
			String permission = OIDFJSON.getString(el);
			if(!CUSTOMER_DATA_PERMISSIONS.contains(permission)){
				extraPermissions.add(permission);
			}
		});

		if(extraPermissions.isEmpty()){
			throw error("The POST Consents request only returned customer data permissions, implying the institution" +
				" do not support any Product that can be used with the Resources API. Make sure that this is the case" +
				" with the tested server, otherwise, re-run the test.");
		}

		logSuccess("Returned permissions array contains not only customer data permissions");

		return env;
	}
}
