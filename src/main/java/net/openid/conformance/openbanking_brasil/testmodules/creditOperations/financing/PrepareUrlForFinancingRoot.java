package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing;

import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFinancingRoot extends ResourceBuilder {

	@Override
	public Environment evaluate(Environment env) {

		setApi("financings");
		setEndpoint("/contracts");

		return super.evaluate(env);
	}
}
