package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpStatus;

public class EnsureResponseCodeWas200or423 extends AbstractCondition {


	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {
		int status = env.getInteger("resource_endpoint_response_full", "status");

		if (status != HttpStatus.OK.value()) {
			log("Status is not 200, checking 423");
			if (status != HttpStatus.LOCKED.value()) {
				throw error("Was expecting either 423 or 200 response");
			} else {
				logSuccess("423 response status, as expected");
			}
		} else {
			logSuccess("200 response status, as expected");
		}

		return env;
	}
}
