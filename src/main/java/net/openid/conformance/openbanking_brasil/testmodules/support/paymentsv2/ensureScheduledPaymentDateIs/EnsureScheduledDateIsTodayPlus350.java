package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs;

public class EnsureScheduledDateIsTodayPlus350 extends AbstractEnsureScheduledDateIsX {

	@Override
	protected long getNumberOfDaysToAddToCurrentDate() {
		return 350L;
	}
}
