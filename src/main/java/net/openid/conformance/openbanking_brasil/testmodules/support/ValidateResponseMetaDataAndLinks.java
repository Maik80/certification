package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.field.*;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateResponseMetaDataAndLinks extends AbstractJsonAssertingCondition {

	public static final String IS_META_OPTIONAL = "is_meta_optional";
	private static final String LINKS_PATTERN = "^(https:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*)$";
	private static final String FIELD_TOTAL_PAGES = "totalPages";
	private static final String FIELD_TOTAL_RECORDS = "totalRecords";

	/**
	 * Validates metadata in any response
	 * An actual full response has to be mapped to the endpoint_response
	 * Be default meta object is mandatory. If is_meta_optional env var is set to true, then meta will be validated only if it is present.
	 */
	@Override
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		Boolean isOptional = Optional.ofNullable(environment.getBoolean(IS_META_OPTIONAL)).orElse(false);

		Object requestData = Optional.ofNullable(body.getAsJsonObject().get("data"))
			.map(d -> {
				if(d.isJsonArray()) {
					return d.getAsJsonArray();
				}
				return d.getAsJsonObject();
			})
			.orElseGet(JsonObject::new);

		JsonObject requestMeta = Optional.ofNullable(body.getAsJsonObject().get("meta"))
			.map(JsonElement::getAsJsonObject)
			.orElseGet(JsonObject::new);

		JsonObject requestLink = Optional.ofNullable(body.getAsJsonObject().get("links"))
			.map(JsonElement::getAsJsonObject)
			.orElseGet(JsonObject::new);

		assertField(body,
			new ObjectField
				.Builder("links")
				.setValidator(o -> assertInnerFields(o, requestMeta))
				.setOptional()
				.build());

		assertField(body,
			new ObjectField
				.Builder("meta")
				.setValidator(o -> assertMeta(o, requestData))
				.setOptional(isOptional)
				.build());

		assertSelfLink(requestLink, requestData);
		return environment;
	}

	public void assertMeta(JsonObject meta, Object data) {
		assertField(meta,
			new IntField
				.Builder(FIELD_TOTAL_RECORDS)
				.build());

		assertField(meta,
			new IntField
				.Builder(FIELD_TOTAL_PAGES)
				.build());

		assertField(meta,
			new StringField
				.Builder("requestDateTime")
				.setMaxLength(20)
				.setPattern(DatetimeField.DEFAULT_PATTERN)
				.build());

		assertMetaAndData(meta, data);
	}

	public void assertInnerFields(JsonObject links, JsonObject meta) {
		assertField(links,
			new StringField
				.Builder("self")
				.setMaxLength(2000)
				.setPattern(LINKS_PATTERN)
				.build());

		assertField(links,
			new StringField
				.Builder("first")
				.setOptional()
				.setPattern(LINKS_PATTERN)
				.setMaxLength(2000)
				.build());

		assertField(links,
			new StringField
				.Builder("prev")
				.setPattern(LINKS_PATTERN)
				.setMaxLength(2000)
				.setOptional()
				.build());

		assertField(links,
			new StringField
				.Builder("next")
				.setPattern(LINKS_PATTERN)
				.setMaxLength(2000)
				.setOptional()
				.build());

		assertField(links,
			new StringField
				.Builder("last")
				.setPattern(LINKS_PATTERN)
				.setMaxLength(2000)
				.setOptional()
				.build());

		assertPrevAndNextLink(links,meta);

	}

	private void assertMetaAndData(JsonObject meta, Object data){
		Boolean hasTotalPages = Optional.ofNullable(JsonHelper.ifExists(meta, FIELD_TOTAL_PAGES)).orElse(false);
		Integer totalPages = 1;
		if(Boolean.TRUE.equals(hasTotalPages)) {
			totalPages = Optional.ofNullable(OIDFJSON.getInt(findByPath(meta, FIELD_TOTAL_PAGES))).orElse(1);
		}
		Boolean hasTotalRecords = Optional.ofNullable(JsonHelper.ifExists(meta, FIELD_TOTAL_RECORDS)).orElse(false);
		Integer totalRecords = 1;
		if(Boolean.TRUE.equals(hasTotalRecords)) {
			totalRecords = Optional.ofNullable(OIDFJSON.getInt(findByPath(meta, FIELD_TOTAL_RECORDS))).orElse(1);
		}
		int arrayCount = 0;
		if(data instanceof JsonArray) {
			arrayCount = ((JsonArray) data).size();
		} else {
			arrayCount = 1;
			List<String> keysWithData = List.of("releases");
			for (Map.Entry<String, JsonElement> stringJsonElementEntry : ((JsonObject) data).entrySet()) {
					if(keysWithData.contains(stringJsonElementEntry.getKey()) && stringJsonElementEntry.getValue().isJsonArray()) {
						arrayCount = stringJsonElementEntry.getValue().getAsJsonArray().size();
					}
			}
		}

		if (arrayCount > totalRecords) {
			throw error("Data contains more items than the metadata totalRecords.");
		}

		if (arrayCount == 0) {
			log("Array is empty");
			if (totalPages == 0 && totalRecords == 0) {
				logSuccess("totalPages and totalRecords are 0 as expected");
			} else {
				throw error("totalPages and totalRecords fields have to be 0 when data array is empty");
			}
		}
	}

	private void assertSelfLink(JsonObject links, Object data) {
		Boolean isConsentRequest = false;
		Boolean isPayment = false;
		Boolean isPaymentConsent = false;
		if(data instanceof JsonObject) {
			isConsentRequest = Optional.ofNullable(JsonHelper.ifExists((JsonObject) data, "consentId")).orElse(false);
			isPaymentConsent = Optional.ofNullable(JsonHelper.ifExists((JsonObject) data, "payment")).orElse(false);
			isPayment = Optional.ofNullable(JsonHelper.ifExists((JsonObject) data, "paymentId")).orElse(false);
		}
		Boolean hasSelfLink = Optional.ofNullable(JsonHelper.ifExists(links, "self")).orElse(false);

		if (Boolean.TRUE.equals(hasSelfLink)) {
			String self = OIDFJSON.getString(findByPath(links, "self"));
			log("Validating self link: " + self);
			if (isConsentRequest && !isPaymentConsent && !isPayment) {
				validateSelfLink(self,
					OIDFJSON.getString(((JsonObject) data).get("consentId")));
			}
		} else {
			//  self link is mandatory for all resources except dados Consents (payment consents do require a self link)
			if (Boolean.FALSE.equals(isConsentRequest)) {
				throw error("There should be a 'self' link.");
			} else {
				if (Boolean.TRUE.equals(isPaymentConsent)) {
					throw error("Payment consent requires a 'self' link.");
				}
			}
		}
	}
	private void assertPrevAndNextLink(JsonObject links, JsonObject meta) {
		String self = OIDFJSON.getString(findByPath(links, "self"));
		Boolean hasPrevLink = Optional.ofNullable(JsonHelper.ifExists(links, "prev")).orElse(false);
		Boolean hasNextLink = Optional.ofNullable(JsonHelper.ifExists(links, "next")).orElse(false);
		Boolean hasTotalPages = Optional.ofNullable(JsonHelper.ifExists(meta, FIELD_TOTAL_PAGES)).orElse(false);
		Integer totalPages = 1;
		if(Boolean.TRUE.equals(hasTotalPages)) {
			totalPages = Optional.ofNullable(OIDFJSON.getInt(findByPath(meta, FIELD_TOTAL_PAGES))).orElse(1);
		}

		if (totalPages <= 1) {
			if (hasPrevLink || hasNextLink) {
				throw error("In the presence of a ‘prev’ or ‘next’ link, ‘totalPages’ must be greater than one, since ‘prev’ and ‘next’ are links to previous or next pages respectively, so that, added to the current page, it must total a quantity greater than one page.",
					args(FIELD_TOTAL_PAGES, totalPages));
			}
		} else {
			List<NameValuePair> queryParams;
			try {
				queryParams = URLEncodedUtils.parse(new URI(self), StandardCharsets.UTF_8);
			} catch (URISyntaxException e) {
				throw error("Invalid 'self' link URI.");
			}

			Integer selfLinkPageNum = Integer.valueOf(queryParams.stream().filter(pair -> pair.getName().equals("page")).findFirst().map(NameValuePair::getValue).orElseGet(() -> "1"));
			if(selfLinkPageNum == 1) {
				if (Boolean.TRUE.equals(hasPrevLink)) {
					throw error("There should not be a 'prev' link.");
				}
				// self link page = 1, total page > 1 - we need a next link.
				if (Boolean.FALSE.equals(hasNextLink)) {
					throw error("There should be a 'next' link.");
				}
			} else {
				if (selfLinkPageNum < totalPages) {
					// Total pages > 1 and self page > 1 and self page < total pages - so we should see a next & prev link
					if (Boolean.FALSE.equals(hasNextLink)) {
						throw error("There should be a 'next' link.");
					}

					if (Boolean.FALSE.equals(hasPrevLink)) {
						throw error("There should be a 'prev' link.");
					}
				}
				if (selfLinkPageNum.equals(totalPages)) {
					if (Boolean.TRUE.equals(hasNextLink)) {
						throw error("There should not be a 'next' link.");
					}

					if (Boolean.FALSE.equals(hasPrevLink)) {
						throw error("There should be a 'prev' link.");
					}
				}
			}
		}
	}

	protected void validateSelfLink(String selfLink, String consentIdField) {
		final Pattern consentRegex = Pattern.compile(String.format("^(https://)(.*?)(consents|payments)(/v\\d/consents/%s)", consentIdField), Pattern.CASE_INSENSITIVE);
		Matcher matcher = consentRegex.matcher(selfLink);
		if (matcher.find()) {
			logSuccess("Consent ID in self link matches the consent ID in the returned object");
		} else {
			throw error("Invalid 'self' link URI. URI: " + selfLink);
		}
	}
}
