package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancelledFrom;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PatchPaymentCancellationFromEnumV2;

import java.util.List;

public class EnsureCancelledFromWasIniciadora extends AbstractEnsureCancelledFromWasX {
	@Override
	protected List<String> getExpectedCancelledFromTypes() {
		return List.of(PatchPaymentCancellationFromEnumV2.INICIADORA.toString());
	}
}
