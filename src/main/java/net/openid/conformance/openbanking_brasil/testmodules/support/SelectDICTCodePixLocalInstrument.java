package net.openid.conformance.openbanking_brasil.testmodules.support;

public class SelectDICTCodePixLocalInstrument extends AbstractPixLocalInstrumentCondition {
	@Override
	protected String getPixLocalInstrument() {
		return "DICT";
	}
}
