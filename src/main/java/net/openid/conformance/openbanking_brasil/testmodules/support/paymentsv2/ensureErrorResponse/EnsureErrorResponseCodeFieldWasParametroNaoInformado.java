package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.CreateConsentErrorEnumV2;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasParametroNaoInformado extends AbstractEnsureErrorResponseCodeFieldWas {
	@Override
	protected List<String> getExpectedCodes() {
		return List.of(CreateConsentErrorEnumV2.PARAMETRO_NAO_INFORMADO.toString());
	}
}
