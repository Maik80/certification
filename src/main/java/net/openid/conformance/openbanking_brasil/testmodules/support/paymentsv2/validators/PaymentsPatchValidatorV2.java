package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators;

import net.openid.conformance.logging.ApiName;

/**
 * Doc https://openbanking-brasil.github.io/openapi/swagger-apis/payments/2.0.0.yml
 * URL: /pix/payments/{paymentId}
 * URL: /pix/payments
 * Api git hash: 127e9783733a0d53bde1239a0982644015abe4f1
 */
@ApiName("Payment Patch Pix V2")
public class PaymentsPatchValidatorV2 extends PaymentInitiationPixPaymentsValidatorV2 {

	@Override
	protected boolean isCancellationOptional() {
		return false;
	}
}
