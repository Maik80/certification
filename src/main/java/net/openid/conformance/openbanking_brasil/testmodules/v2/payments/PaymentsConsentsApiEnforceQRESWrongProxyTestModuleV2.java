package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.InjectRealCreditorAccountEmailToPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.InjectRealCreditorAccountToPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRESCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectQRCodeWithWrongEmailAddressProxyIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRESCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetProxyToRealEmailAddressOnPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SetProxyToRealEmailAddressOnPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasDetalhePagamentoInvalido;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments-api-qres-mismatched-proxy-test-v2",
	displayName = "Ensure that payment fails when the the PIX key sent on the Static QR Code differs from proxy key sent (Reference Error 2.2.2.8)",
	summary = "Ensure that payment fails when the PIX key sent on the Static QR Code differs from proxy key sent (Reference Error 2.2.2.8)\n" +
		"\u2022 Call POST Consent with different Pix Key on QRES and proxy key on the payload \n" +
		"\u2022 Expects 422 - DETALHE_PAGAMENTO_INVALIDO\n" +
		"\u2022 Validate Error message\n" +
		"If no errors are identified:\n" +
		"\u2022 Redirects the user to authorize the created consent \n" +
		"\u2022 Call GET Consent \n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" \n" +
		"\u2022 Calls the POST Payments Endpoint \n" +
		"\u2022 Expects 422 - DETALHE_PAGAMENTO_INVALIDO \n" +
		"\u2022 Validate Error message \n" +
		"If no errors are identified: \n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expects a payment with the definitive state (RJCT) status and RejectionReason equal to DETALHE_PAGAMENTO_INVALIDO\n" +
		"\u2022 Validate Error message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiEnforceQRESWrongProxyTestModuleV2 extends AbstractPaymentConsentUnhappyPathTestModuleV2 {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectQRESCodeLocalInstrument.class);
		callAndStopOnFailure(SelectQRESCodePixLocalInstrument.class);
		callAndStopOnFailure(InjectQRCodeWithWrongEmailAddressProxyIntoConfig.class);
		callAndStopOnFailure(SetProxyToRealEmailAddressOnPaymentConsent.class);
		callAndStopOnFailure(SetProxyToRealEmailAddressOnPayment.class);
		callAndStopOnFailure(InjectRealCreditorAccountEmailToPaymentConsent.class);
		callAndStopOnFailure(InjectRealCreditorAccountToPayment.class);
	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasDetalhePagamentoInvalido.class);
	}

	@Override
	protected void validate422ErrorResponseCode() {
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class);
	}

	@Override
	protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		return EnsureResourceResponseCodeWas201Or422.class;
	}

}
