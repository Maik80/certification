package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CallDirectoryParticipantsEndpointFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.ExtractAuthorisationServerFromParticipantsEndpoint;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class ValidateCaChainReturnedSequence extends AbstractConditionSequence {
	@Override
	public void evaluate() {
		callAndStopOnFailure(CallDirectoryRootsEndpoint.class);
		callAndStopOnFailure(CallDirectoryParticipantsEndpointFromConfig.class);
		callAndStopOnFailure(ExtractAuthorisationServerFromParticipantsEndpoint.class);
		callAndContinueOnFailure(ValidateCaChainReturnedPhase2Phase3.class, Condition.ConditionResult.FAILURE);
	}
}
