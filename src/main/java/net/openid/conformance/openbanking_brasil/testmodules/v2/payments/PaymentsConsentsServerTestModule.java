package net.openid.conformance.openbanking_brasil.testmodules.v2.payments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.ExtractMTLSCertificatesFromConfiguration;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractBlockLoggingTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.ValidateCaChainReturnedSequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.*;

@PublishTestModule(
	testName = "fvp-payments-consents-server-certificate-v2",
	displayName = "Payments Consents Server Certificate - FVP",
	summary = "This test aims to confirm if the Certificate Used on the Server Endpoints is aligned with the Brazil Certificate Standards \n" +
		"\n" +
		"\u2022 Obtain all the base mtls Functional URI endpoints set on major version 2 tests the server from the directory using the directory participants API and the provided discoveryUrl. At Most 12 Functional Endpoints are to be obtained : consents/v2/consents, accounts/v2/accounts, loans/v2/contracts, credit-cards-accounts/v2/accounts, customers/v2/personal/identifications, customers/v2/business/identifications, financings/v2/contracts, invoice-financings/v2/contracts, resources/v2/resources, unarranged-accounts-overdraft/v2/contracts, payments/v2/pix/payments and payments/v2/consents \n" +
		"\n" +
		"\u2022 Extract that the Token Endpoint Server Certificate - Validate the certificate chain follows the RFC5246 - 7.4.2 clause. Validate that the leaf certificate sent is issued from one of the Accepted OPF CAs \n" +
		"\n" +
		"\u2022 Extract that the Registration Endpoint Server Certificate - Validate the certificate chain follows the RFC5246 - 7.4.2 clause. Validate that the leaf certificate sent is issued from one of the Accepted OPF CAs\n" +
		"\n" +
		"\u2022 Extract, for all the registered functional endpoints, the Endpoint Server Certificate - Validate the certificate chain follows the RFC5246 - 7.4.2 clause. Validate that the leaf certificate sent is issued from one of the Accepted OPF CAs\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.directoryRootsUri",
		"directory.participants"
	}
)
@VariantNotApplicable(parameter = FAPI1FinalOPProfile.class, values = {"openbanking_uk", "plain_fapi", "consumerdataright_au"})
@VariantParameters({
	ClientAuthType.class,
	FAPI1FinalOPProfile.class,
	FAPIResponseMode.class
})
@VariantNotApplicable(parameter = ClientAuthType.class, values = {
	"none", "client_secret_basic", "client_secret_post", "client_secret_jwt"
})
public class PaymentsConsentsServerTestModule extends AbstractBlockLoggingTestModule {
	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {
		env.putObject("config", config);
		callAndStopOnFailure(ExtractMTLSCertificatesFromConfiguration.class);
		setStatus(Status.CONFIGURED);
	}

	@Override
	public void start() {
		setStatus(Status.RUNNING);
		callAndStopOnFailure(AddResourceUrlToConfig.class);
		ConditionSequence caSequence = new ValidateCaChainReturnedSequence();
		call(caSequence);
		fireTestFinished();
	}
}
