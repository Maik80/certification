package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public abstract class AbstractEnsurePaymentStatusWasX extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	private String status;

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		List<String> expectedStatuses = getExpectedStatuses();
		if (!checkPaymentStatus(env)) {
			throw error("Payment status is not what was expected", args("Expected", expectedStatuses, "Actual", status));
		}
		logSuccess("Payment status is what was expected", args("Expected", expectedStatuses, "Status", status));
		return env;
	}

	protected boolean checkPaymentStatus(Environment env) {
		try {
			JsonObject responseBody = BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response")).getAsJsonObject();

			JsonObject data = Optional.ofNullable(responseBody.getAsJsonObject("data"))
				.orElseThrow(() -> error("Could not extract data from body", args("body", responseBody))).getAsJsonObject();

			status = OIDFJSON.getString(Optional.ofNullable(data.get("status"))
				.orElseThrow(() -> error("Could not extract status from data", args("data", data))));

			List<String> expectedStatuses = getExpectedStatuses();

			return expectedStatuses.contains(status);


		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	protected abstract List<String> getExpectedStatuses();


	public String getStatus() {
		return status;
	}
}
