package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class SetIncorrectEndToEndIdTimeInThePaymentRequestBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject paymentRequestData = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.flatMap(request -> Optional.ofNullable(request.getAsJsonObject().getAsJsonObject("data")))
			.orElseThrow(() -> error("Could not extract brazilPixPayment.data from the resource"))
			.getAsJsonObject();

		String originalEndToEndId = OIDFJSON.getString(Optional.ofNullable(paymentRequestData.get("endToEndId"))
			.orElseThrow(() -> error("Could not extract endToEndId from the brazilPixPayment.data", args("data", paymentRequestData))));

		String endToEndIdStart = originalEndToEndId.substring(0, 17);
		String endToEndIdEnd = originalEndToEndId.substring(21);
		String newEndToEndId = endToEndIdStart + "0100" + endToEndIdEnd;

		paymentRequestData.addProperty("endToEndId", newEndToEndId);

		logSuccess("Incorrect end to end Id was added to the brazilPixPayment",
			args("original", originalEndToEndId, "new", newEndToEndId, "data", paymentRequestData));

		return env;
	}
}
