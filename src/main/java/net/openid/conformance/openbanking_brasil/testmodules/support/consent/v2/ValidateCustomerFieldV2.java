package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class ValidateCustomerFieldV2 extends AbstractValidateField {
    @Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {

		JsonElement config = env.getObject("config");
		setConsentVersion(CONSENT_VERSION_2);
		validateMainFields(config);
		return env;
	}
}
