package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.util.X509CertUtils;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.AbstractCallProtectedResource;
import net.openid.conformance.condition.util.MtlsKeystoreBuilder;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.ManagedHttpClientConnection;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.protocol.HttpCoreContext;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.*;

public abstract class AbstractValidateCaChainReturned extends AbstractCallProtectedResource {
	private TestInstanceEventLog log;

	@Override
	@PreEnvironment(required = {"config", "directory_roots_response", "authorisation_server"})
	public Environment evaluate(Environment env) {
		JsonObject authorisationServer = env.getObject("authorisation_server");
		if (authorisationServer == null){
			throw error("Authorisation Server not present in environment test unable to continue");
		}
		String versionRegex = getVersionRegex();

		List<String> endpointRegexes = getEndpointRegexes();

		List<String> endpoints = getEndpointList(authorisationServer,versionRegex,endpointRegexes);

		endpoints.forEach(endpoint -> callEndPoint(env, endpoint));

		return env;
	}

	private void callEndPoint(Environment env, String resourceUrl){
		try {
			RestTemplate restTemplate = createRestTemplate(env);

			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public boolean hasError(ClientHttpResponse response) throws IOException {
					// Treat all http status codes as 'not an error', so spring never throws an exception due to the http
					// status code meaning the rest of our code can handle http status codes how it likes
					return false;
				}
			});

			HttpMethod method = HttpMethod.GET;
			HttpHeaders headers = getHeaders(env);

			if (headers.getAccept().isEmpty()) {
				headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			}

			HttpEntity<?> request = new HttpEntity<>(getBody(env), headers);
			restTemplate.exchange(resourceUrl, method, request, String.class);

		} catch (UnrecoverableKeyException | KeyManagementException | CertificateException | InvalidKeySpecException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
			throw error("Error creating HTTP client", e);
		} catch (RestClientException e) {
			String msg = "Call to protected resource " + resourceUrl + " failed";
			if (e.getCause() != null) {
				msg += " - " +e.getCause().getMessage();
			}
			throw error(msg, e);
		}
	}
	@Override
	protected HttpClient createHttpClient(Environment env, boolean restrictAllowedTLSVersions)
		throws CertificateException, InvalidKeySpecException, NoSuchAlgorithmException,
		KeyStoreException, IOException, UnrecoverableKeyException, KeyManagementException {


		HttpClientBuilder builder = HttpClientBuilder.create()
			.useSystemProperties()
			.addInterceptorFirst((HttpResponseInterceptor) (httpResponse, httpContext) -> {
				ManagedHttpClientConnection routedConnection = (ManagedHttpClientConnection) httpContext.getAttribute(HttpCoreContext.HTTP_CONNECTION);
				SSLSession sslSession = routedConnection.getSSLSession();
				if (sslSession != null) {
					Certificate[] certificates = sslSession.getPeerCertificates();
					if (certificates == null){
						throw error("No Certificate Chain sent by the tested server, which is not in compliance with security specifications definitions “The server certificate shall be sent with the intermediate chain, according to [RFC5246] (items 7.4.2).");
					}
					validateLeafAgainstDirectoryRoots(env, certificates);
					validateTrustChainReturned(certificates);
				} else {
					throw error("No SSL Connection detected");
				}
			});

		int timeout = 60; // seconds
		RequestConfig config = RequestConfig.custom()
			.setConnectTimeout(timeout * 1000)
			.setConnectionRequestTimeout(timeout * 1000)
			.setSocketTimeout(timeout * 1000).build();
		builder.setDefaultRequestConfig(config);

		KeyManager[] km = null;

		// initialize MTLS if it's available
		String[] suites = null;
		if (env.containsObject("mutual_tls_authentication")) {
			km = MtlsKeystoreBuilder.configureMtls(env, log);
			suites = new String[] {"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"};
		}

		TrustManager[] trustAllCerts = {
			new X509TrustManager() {

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}
			}
		};

		SSLContext sc = SSLContext.getInstance("TLS");
		sc.init(km, trustAllCerts, new SecureRandom());

		builder.setSSLContext(sc);


		SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sc,
			(restrictAllowedTLSVersions ? new String[] { "TLSv1.2" } : null),
			suites,
			NoopHostnameVerifier.INSTANCE);

		builder.setSSLSocketFactory(sslConnectionSocketFactory);

		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
			.register("https", sslConnectionSocketFactory)
			.register("http", new PlainConnectionSocketFactory())
			.build();

		HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
		builder.setConnectionManager(ccm);

		builder.disableRedirectHandling();

		builder.disableAutomaticRetries();

		HttpClient httpClient = builder.build();
		return httpClient;
	}

	private void validateTrustChainReturned(Certificate... certificates){
		try {
			List<X509Certificate> certificateList = new ArrayList<>();
			for (Certificate certificate : certificates) {
				certificateList.add(X509CertUtils.parse(certificate.getEncoded()));
			}
			Set<TrustAnchor> trustAnchors = generateTrustAnchors(certificateList);
			PKIXBuilderParameters params = generatePixParams(certificates, trustAnchors);

			//validate leaf
			CertPathBuilder certPathBuilder = CertPathBuilder.getInstance("PKIX");
			certPathBuilder.build(params);


			CertPathValidator cpv = CertPathValidator.getInstance("PKIX");
			PKIXParameters newParams = new PKIXParameters(trustAnchors);
			newParams.setRevocationEnabled(false);

			CertificateFactory cf = CertificateFactory.getInstance("X509");
			CertPath path = cf.generateCertPath(certificateList);
			//validateChain
			cpv.validate(path, params);
		} catch (CertificateException e) {
			logFailure("Cannot validate that certificate sent is certified by the trust chain sent, in accordance to RFC5246 item 7.4.2.");
		} catch (CertPathValidatorException e){
			List<String> certPath;
			if (e.getCertPath() != null){
				certPath = decodeChainToPemString(e.getCertPath().getCertificates());
				logFailure("Cannot validate that the trust chain sent is valid, in accordance to RFC5246 item 7.4.2.", args(e.getReason().toString(), certPath.toString()));
			} else {
				logFailure("Cannot validate that the trust chain sent is valid, in accordance to RFC5246 item 7.4.2.", args(e.getReason().toString(),"No Cert Path Available"));
			}
		} catch (CertPathBuilderException |InvalidAlgorithmParameterException | NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
	private void validateLeafAgainstDirectoryRoots(Environment env, Certificate... certificates){
		JsonObject response = env.getObject("directory_roots_response");
		if (response == null){
			throw error("Directory Roots Response is empty test failing");
		}
		JsonElement directoryRootsResponse = response.get("body");
		JsonObject jwkObject = new Gson().fromJson(OIDFJSON.getString(directoryRootsResponse),JsonObject.class);
		JsonArray caArray = (JsonArray) jwkObject.get("keys");
		try {

			List<X509Certificate> caCerts = new ArrayList<>();
			caArray.forEach(jwk -> {
				List<X509Certificate> jwkChain = extractCertificateFromJwk(jwk);
				jwkChain.forEach(jwkCert -> {
					caCerts.add(jwkCert);
				});
			});
			Set<TrustAnchor> trustAnchors = generateTrustAnchors(caCerts);
			//set leaf to validate against CA's
			PKIXBuilderParameters params = generatePixParams(certificates, trustAnchors);
			//validate leaf
			CertPathBuilder certPathBuilder = CertPathBuilder.getInstance("PKIX");
			certPathBuilder.build(params);
			logSuccess("Successfully validated the leaf certificate returned by the resource server against the trust chain published on the respective TrustPlatform directory");
		} catch (CertPathBuilderException e) {
			logFailure("Cannot validate that certificate sent is certified by the trust chain published on the respective TrustPlatform directory, in accordance to RFC5246 item 7.4.2.");
		} catch (CertificateException |InvalidAlgorithmParameterException | NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
	@Override
	protected Environment handleClientResponse(Environment env, JsonObject responseCode, String responseBody, JsonObject responseHeaders, JsonObject fullResponse) {
		//Not interested in the api response
		return null;
	}
	private PKIXBuilderParameters generatePixParams(Certificate[] certificates, Set<TrustAnchor> trustAnchors) throws InvalidAlgorithmParameterException, CertificateEncodingException {
		X509CertSelector selector = new X509CertSelector();
		selector.setCertificate(X509CertUtils.parse(certificates[0].getEncoded()));
		PKIXBuilderParameters params = new PKIXBuilderParameters(trustAnchors, selector);
		params.setRevocationEnabled(false);

		return params;
	}
	private Set<TrustAnchor> generateTrustAnchors(List<X509Certificate> caCerts){
		Set<TrustAnchor> trustAnchors = new HashSet<>();
		caCerts.forEach(cert -> {
			try {
				trustAnchors.add(new TrustAnchor(X509CertUtils.parse(cert.getEncoded()),null));
			} catch (CertificateEncodingException e) {
				throw error(e);
			}
		});
		return trustAnchors;
	}
	private List<X509Certificate> extractCertificateFromJwk(JsonElement jwk){
		JWK parsedJwk;
		try {
			parsedJwk = JWK.parse(jwk.toString());
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}

		return parsedJwk.getParsedX509CertChain();
	}

	private List<String> decodeChainToPemString(List<? extends Certificate> certificates){
		List<String> pemStrings = new ArrayList<>();
		certificates.forEach(certificate -> {
			try {
				X509Certificate certToAdd = X509CertUtils.parse(certificate.getEncoded());
				pemStrings.add(X509CertUtils.toPEMString(certToAdd));
			} catch (CertificateEncodingException e) {
				throw new RuntimeException(e);
			}
		});
		return pemStrings;
	}

	private List<String> getEndpointList(JsonObject authorisationServer, String versionRegex, List<String> endpointRegexes){
		JsonArray apiResources = authorisationServer.getAsJsonArray("ApiResources");
		List<String> endpointToCallList = new ArrayList<>();

		if (apiResources != null){
			endpointRegexes.forEach((endpointRegex) -> {
				for (JsonElement entry : apiResources) {
					JsonObject apiResource = entry.getAsJsonObject();
					if (OIDFJSON.getString(apiResource.get("ApiVersion")).matches(versionRegex)){
						JsonArray apiDiscoveryEndpoints = apiResource.getAsJsonArray("ApiDiscoveryEndpoints");
						for (JsonElement apiEndpoint: apiDiscoveryEndpoints){
							JsonObject apiEndpointObject = apiEndpoint.getAsJsonObject();
							String endpointString = OIDFJSON.getString(apiEndpointObject.get("ApiEndpoint"));
							if (endpointString.matches(endpointRegex)){
								endpointToCallList.add(endpointString);
								logSuccess("Successfully found api endpoint: " + endpointString);
							}
						}
					}
				}
			});
		} else {
			throw error("Authorisation Servers does not have any endpoints available");
		}
		if (endpointToCallList.isEmpty()){
			throw error("Unable to locate at least one of the defined endpoints", args("Endpoints Expected", endpointRegexes));
		}
		logSuccess("Validated Authorisation Server contains API endpoint(s) supporting the correct version of the API", args("Endpoints", endpointToCallList));
		return endpointToCallList;
	}

	protected abstract List<String> getEndpointRegexes();

	protected abstract String getVersionRegex();
}
