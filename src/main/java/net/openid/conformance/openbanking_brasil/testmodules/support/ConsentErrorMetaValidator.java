package net.openid.conformance.openbanking_brasil.testmodules.support;

public class ConsentErrorMetaValidator extends AbstractErrorMetaValidator {
	@Override
	protected boolean isResource() {
		return false;
	}
}
