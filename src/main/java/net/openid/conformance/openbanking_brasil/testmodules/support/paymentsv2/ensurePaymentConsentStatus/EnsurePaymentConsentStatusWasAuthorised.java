package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentConsentStatusEnumV2;

public class EnsurePaymentConsentStatusWasAuthorised extends AbstractEnsurePaymentConsentStatusWas {

	@Override
	protected String getExpectedStatus() {
		return PaymentConsentStatusEnumV2.AUTHORISED.toString();
	}
}
