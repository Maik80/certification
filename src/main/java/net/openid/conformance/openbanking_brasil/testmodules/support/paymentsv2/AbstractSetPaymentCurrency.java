package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public abstract class AbstractSetPaymentCurrency extends AbstractCondition {
	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject payment = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent.data.payment"))
			.orElseThrow(() -> error("Could not extract brazilPaymentConsent.data.payment from resource")).getAsJsonObject();

		payment.addProperty("currency", getCurrency());
		logSuccess("Inserted currency into the payment", args("payment", payment, "currency", getCurrency()));
		return env;
	}

	protected abstract String getCurrency();
}
