package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;


import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas200;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class ValidateRegisteredEndpoints extends AbstractConditionSequence {
	@Override
	public void evaluate() {
			call(exec().startBlock("Validating Authorisation Server has supported Endpoint"));
			callAndStopOnFailure(CallDirectoryParticipantsEndpointFromConfig.class);
			call(exec().mapKey("resource_endpoint_response_full", "directory_participants_response_full"));
			callAndStopOnFailure(EnsureResponseCodeWas200.class);
			call(exec().unmapKey("resource_endpoint_response_full"));
			callAndStopOnFailure(ExtractAuthorisationServerFromParticipantsEndpoint.class);
			callAndStopOnFailure(ValidateConsentsAndResourcesEndpoints.class);
			call(exec().endBlock());
	}
}
