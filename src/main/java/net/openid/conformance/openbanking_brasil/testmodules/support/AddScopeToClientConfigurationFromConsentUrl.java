package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class AddScopeToClientConfigurationFromConsentUrl extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonElement consentUrlElement = Optional.ofNullable(env.getElementFromObject("config", "resource.consentUrl"))
			.orElseThrow(() -> error("Could not find consent URI in the client configuration"));
		String consentUrl = OIDFJSON.getString(consentUrlElement);

		String consentRegex = "^(https://)(.*?)(consents/v[0-9]/consents)";
		String paymentsConsentsRegex = "^(https://)(.*?)(payments/v[0-9]/consents)";

		String scopeToBeAdded;
		if(consentUrl.matches(consentRegex)){
			scopeToBeAdded = "consents";
		}else if(consentUrl.matches(paymentsConsentsRegex)){
			scopeToBeAdded = "payments";
		}else {
			throw error("Invalid consent URI was provided", args("consent URI", consentUrl));
		}

		env.putString("config", "client.scope", scopeToBeAdded);

		logSuccess("Scope was added to the client config", args("scope", scopeToBeAdded));

		return env;
	}
}
