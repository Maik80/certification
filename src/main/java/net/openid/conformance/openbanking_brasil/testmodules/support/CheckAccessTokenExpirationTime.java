package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class CheckAccessTokenExpirationTime extends AbstractCondition {


	@Override
	@PreEnvironment(strings = "timestamp")
	public Environment evaluate(Environment env) {
		JsonElement expirationSecondsElement = env.getElementFromObject("token_endpoint_response", "expires_in");
		if (expirationSecondsElement == null) {
			throw error("Could not find expiration time in the token response");
		}
		long expirationSeconds = OIDFJSON.getLong(expirationSecondsElement);
		LocalTime tokenEndpointRequestTime = LocalTime.parse(env.getString("timestamp"), DateTimeFormatter.ISO_LOCAL_TIME);
		LocalTime currentTime = LocalTime.now();

		long deltaSeconds = Duration.between(tokenEndpointRequestTime, currentTime).toSeconds();

		float f = (float) deltaSeconds / (float) expirationSeconds;

		if(f > 0.9f){
			env.putBoolean("refresh", true);
		}

		return env;
	}
}
