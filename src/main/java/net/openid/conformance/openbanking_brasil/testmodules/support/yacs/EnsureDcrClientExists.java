package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EnsureDcrClientExists extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String cliendId = getStringFromEnvironment(env,"config","client.client_id","Config DCR Client Id");
		String preRollClientId = getStringFromEnvironment(env,"config","dcr_client_id","Pre-test DCR Client Id");
			if (cliendId.equals(preRollClientId)){
				logSuccess("Correct DCR Client being used", args("client_id", cliendId));
				return env;
			} else {
				throw error("Incorrect DCR client being used, test stopping");
			}
		}
}
