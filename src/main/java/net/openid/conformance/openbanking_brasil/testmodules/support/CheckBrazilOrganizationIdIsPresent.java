package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CheckBrazilOrganizationIdIsPresent extends AbstractCondition {

	private static final String ENV_KEY = "config";

	@Override
	@PreEnvironment(required = ENV_KEY)
	public Environment evaluate(Environment env) {
		String brazilOrganizationId = env.getString(ENV_KEY, "resource.brazilOrganizationId");
		if (Strings.isNullOrEmpty(brazilOrganizationId)) {
			throw error("Could not find the organization Id on the config");
		}
		logSuccess("Successfully found organization Id: " + brazilOrganizationId);
		return env;
	}
}
