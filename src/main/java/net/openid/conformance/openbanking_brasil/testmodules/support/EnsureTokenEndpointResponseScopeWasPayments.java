package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class EnsureTokenEndpointResponseScopeWasPayments extends AbstractCondition {

	@Override
	@PreEnvironment(required = "token_endpoint_response")
	public Environment evaluate(Environment env) {
		JsonElement scopeElement = env.getElementFromObject("token_endpoint_response", "scope");
		if (scopeElement == null) {
			throw error("The response from token endpoint does not have a scope field.");
		}
		String scope = OIDFJSON.getString(scopeElement);
		if (scope.equals("payments")) {
			logSuccess("The scope field from token endpoint response is set to 'payments', as expected.");
		} else {
			throw error("The scope field from token endpoint response is not set to 'payments'.", args("scope", scope));
		}
		return env;
	}
}
