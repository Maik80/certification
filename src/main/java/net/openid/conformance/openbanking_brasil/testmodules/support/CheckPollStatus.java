package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractCheckPaymentPollStatus;

import java.util.List;

public class CheckPollStatus extends AbstractCheckPaymentPollStatus {

	@Override
	protected List<String> getExpectedStatuses() {
		return List.of("PDNG", "PART");
	}
}
